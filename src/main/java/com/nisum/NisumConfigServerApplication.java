package com.nisum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigServer
@RefreshScope
public class NisumConfigServerApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(NisumConfigServerApplication.class, args);
	}
}
