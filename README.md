
Nisum Configuration Server is defined to load application-related properties files from remote repositories based on profiles you choose while launching the application.


### How do I get set up? ###

* Clone the project either with IDE or git command line
* To launch the application 
     - Command line: go to project directory (nisum-config-server) and execute /.gradlew bootRun command.
     - IDE: Right-click on the project and add Gradle nature to it; right-click and choose Run As -> Spring Boot App option.


### Contribution guidelines ###

* Make sure that you need to write Junit test cases which need to cover 100% code coverage.
* Code review needs to be done before pushing the code.


### Who do I talk to? ###

SVadikari@gmail.com